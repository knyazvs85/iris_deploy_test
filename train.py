from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score
import pickle

dataset = datasets.load_iris()

data = dataset.data
target = dataset.target

X_train, X_test, y_train, y_test = train_test_split(data, target, test_size=0.1, random_state=42, shuffle=True)

model = RandomForestClassifier(n_estimators=1, max_depth=2)

model.fit(X_train, y_train)
pred_train = model.predict(X_train)
pred_test = model.predict(X_test)

print("train_f1", f1_score(y_train, pred_train, average="macro"), "test_f1", f1_score(y_test, pred_test, average="macro"))

pickle.dump(model, open('model.pickle', "wb"))

model = pickle.load(open("model.pickle", "rb"))
print("loaded model score", f1_score(y_test, model.predict(X_test), average="macro"))